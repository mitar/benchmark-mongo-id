const BASE58 = '123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz';

const crypto = require('crypto');
const { MongoClient } = require('mongodb');
const bs58 = require('base-x')(BASE58);

const URI = 'mongodb://localhost?retryWrites=true&w=majority';

const client = new MongoClient(URI);

const pickStats = ({ns, size, count, avgObjSize, storageSize, totalIndexSize, totalSize, indexSizes}) => ({ns, size, count, avgObjSize, storageSize, totalIndexSize, totalSize, indexSizes});

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function run() {
  try {
    await client.connect();
    const database = client.db('benchmark');
    const binary = database.collection('binary');
    const string = database.collection('string');

    console.log("Preparing data to insert.");
    const binaryIds = [];
    const stringIds = [];
    for (let i = 0; i < 100000; i++) {
      const binaryId = crypto.randomBytes(16);
      binaryIds.push({
        _id: binaryId,
      });
      stringIds.push({
        _id: bs58.encode(binaryId),
      });
    }

    console.log("Inserting.");
    await binary.insertMany(binaryIds);
    await string.insertMany(stringIds);

    // We have to wait for a bit before stats return real numbers.
    // See: https://jira.mongodb.org/browse/WT-6887
    console.log("Waiting for a bit.");
    await sleep(60 * 1000);

    console.log("Done.");
    const binaryStats = await binary.stats();
    const stringStats = await string.stats();

    console.dir(pickStats(binaryStats));
    console.dir(pickStats(stringStats));
  } finally {
    await client.close();
  }
}

run().catch(console.dir);
